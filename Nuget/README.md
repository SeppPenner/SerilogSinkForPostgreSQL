SerilogSinkForPostgreSQL
====================================

SerilogSinkForPostgreSQL is a library to save logging information from [Serilog](https://github.com/serilog/serilog) to [PostgreSQL](https://www.postgresql.org/).
The assembly was written and tested in .Net Framework 4.8 and .Net Standard 2.0.

[![Build status](https://ci.appveyor.com/api/projects/status/0ggd9vc0fw9gc92c?svg=true)](https://ci.appveyor.com/project/SeppPenner/serilogsinkforpostgresql)
[![GitHub issues](https://img.shields.io/github/issues/SeppPenner/SerilogSinkForPostgreSQL.svg)](https://github.com/SeppPenner/SerilogSinkForPostgreSQL/issues)
[![GitHub forks](https://img.shields.io/github/forks/SeppPenner/SerilogSinkForPostgreSQL.svg)](https://github.com/SeppPenner/SerilogSinkForPostgreSQL/network)
[![GitHub stars](https://img.shields.io/github/stars/SeppPenner/SerilogSinkForPostgreSQL.svg)](https://github.com/SeppPenner/SerilogSinkForPostgreSQL/stargazers)
[![License: MIT](https://img.shields.io/badge/License-MIT-blue.svg)](https://raw.githubusercontent.com/SeppPenner/SerilogSinkForPostgreSQL/master/License.txt)
[![Nuget](https://img.shields.io/badge/SerilogSinkForPostgreSQL-Nuget-brightgreen.svg)](https://www.nuget.org/packages/HaemmerElectronics.SeppPenner.SerilogSinkForPostgreSQL/)
[![NuGet Downloads](https://img.shields.io/nuget/dt/HaemmerElectronics.SeppPenner.SerilogSinkForPostgreSQL.svg)](https://www.nuget.org/packages/HaemmerElectronics.SeppPenner.SerilogSinkForPostgreSQL/)
[![Known Vulnerabilities](https://snyk.io/test/github/SeppPenner/SerilogSinkForPostgreSQL/badge.svg)](https://snyk.io/test/github/SeppPenner/SerilogSinkForPostgreSQL)
[![Gitter](https://badges.gitter.im/SerilogSinkForPostgreSQL/community.svg)](https://gitter.im/SerilogSinkForPostgreSQL/community?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge)

## Available for
* NetFramework 4.6.2
* NetFramework 4.7
* NetFramework 4.7.2
* NetFramework 4.8
* NetStandard 2.0
* NetStandard 2.1
* NetCore 2.1
* NetCore 3.0
* NetCore 3.1

## Net Core and Net Framework latest and LTS versions
* https://dotnet.microsoft.com/download/dotnet-framework
* https://dotnet.microsoft.com/download/dotnet-core

## Basic usage:
Check out the how to use file [here](https://github.com/SeppPenner/SerilogSinkForPostgreSQL/blob/master/HowToUse.md).

## Further information:
This project is a fork of https://github.com/b00ted/serilog-sinks-postgresql but is maintained.
Do not hesitate to create [issues](https://github.com/SeppPenner/SerilogSinkForPostgreSQL/issues) or [pull requests](https://github.com/SeppPenner/SerilogSinkForPostgreSQL/pulls).

Change history
--------------

* **Version 1.5.0.0 (2020-02-09)** : Updated nuget packages, updated available versions.
* **Version 1.4.0.0 (2019-11-08)** : Updated nuget packages.
* **Version 1.0.3.0 (2019-06-23)** : Added icon to the nuget package.
* **Version 1.0.2.0 (2019-05-13)** : Updated documentation, fixed some tests.
* **Version 1.0.1.0 (2019-05-08)** : Updated documentation, added documentation to the nuget package and all classes, added option to allow upper case table and column names.
Simplified building and packing scripts. Added support for NetFramework 4.6, NetFramework 4.6.2, NetFramework 4.7 and NetFramework 4.8.
* **Version 1.0.0.0 (2019-02-22)** : 1.0 release.
